void setup() {
  // put your setup code here, to run once:
  pinMode(36, INPUT);
  pinMode(39, INPUT);
  Serial.begin(115200);
  pinMode(5, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  
  Serial.println(digitalRead(36));
  digitalWrite(5, 1-digitalRead(36)); 
  int analog = analogRead(39);
  int vout = map(analog, 0, 4095, 0, 33);
  int vbat = vout*2;
  Serial.println(vbat);
  Serial.println();
  delay(1000);
}
