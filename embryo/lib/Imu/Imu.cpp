#include <Arduino.h>
#include <Imu.h>


bool Imu::init(){
  Serial.println("IMU: Init");
  IMUChip.begin();
  return true;
}

void Imu::dump(){
  //Get all parameters
  Serial.print("\nAccelerometer:\n");
  Serial.print(" X = ");
  Serial.println(IMUChip.readFloatAccelX(), 4);
  Serial.print(" Y = ");
  Serial.println(IMUChip.readFloatAccelY(), 4);
  Serial.print(" Z = ");
  Serial.println(IMUChip.readFloatAccelZ(), 4);

  Serial.print("\nGyroscope:\n");
  Serial.print(" X = ");
  Serial.println(IMUChip.readFloatGyroX(), 4);
  Serial.print(" Y = ");
  Serial.println(IMUChip.readFloatGyroY(), 4);
  Serial.print(" Z = ");
  Serial.println(IMUChip.readFloatGyroZ(), 4);

  Serial.print("\nThermometer:\n");
  Serial.print(" Degrees C = ");
  Serial.println(IMUChip.readTempC(), 4);
  Serial.print(" Degrees F = ");
  Serial.println(IMUChip.readTempF(), 4);
}

rawImu Imu::getRaw(){
  rawImu raw;
  raw.gx = IMUChip.readFloatGyroX();
  raw.gy = IMUChip.readFloatGyroY();
  raw.gz = IMUChip.readFloatGyroZ();

  raw.ax = IMUChip.readFloatAccelX();
  raw.ay = IMUChip.readFloatAccelY();
  raw.az = IMUChip.readFloatAccelZ();

  return raw;
}

fusionImu Imu::getOrientation(){
  rawImu raw = getRaw();
  fusionImu fusion;
  deltat = imuFusion.deltatUpdate();
  imuFusion.MahonyUpdate(raw.gx*DEG_TO_RAD, raw.gy*DEG_TO_RAD, raw.gz*DEG_TO_RAD, raw.ax, raw.ay, raw.az, deltat);

  fusion.pitch = imuFusion.getPitch();
  fusion.roll = imuFusion.getRoll();    //you could also use getRollRadians() ecc
  fusion.yaw = imuFusion.getYaw();

  return fusion;
}

float Imu::getTemp(){
  return IMUChip.readTempC();
}

// #include <Arduino.h>
// #include <Imu.h>
//
//
// bool Imu::init(){
//   Serial.println("IMU: Init");
//   IMUChip.begin();
//   return true;
// }
//
// void Imu::dump(){
//   //Get all parameters
//   Serial.print("\nAccelerometer:\n");
//   Serial.print(" X = ");
//   Serial.println(IMUChip.readFloatAccelX(), 4);
//   Serial.print(" Y = ");
//   Serial.println(IMUChip.readFloatAccelY(), 4);
//   Serial.print(" Z = ");
//   Serial.println(IMUChip.readFloatAccelZ(), 4);
//
//   Serial.print("\nGyroscope:\n");
//   Serial.print(" X = ");
//   Serial.println(IMUChip.readFloatGyroX(), 4);
//   Serial.print(" Y = ");
//   Serial.println(IMUChip.readFloatGyroY(), 4);
//   Serial.print(" Z = ");
//   Serial.println(IMUChip.readFloatGyroZ(), 4);
//
//   Serial.print("\nThermometer:\n");
//   Serial.print(" Degrees C = ");
//   Serial.println(IMUChip.readTempC(), 4);
//   Serial.print(" Degrees F = ");
//   Serial.println(IMUChip.readTempF(), 4);
// }
//
// void Imu::updateRaw(){
//   gx = IMUChip.readFloatGyroX();
//   gy = IMUChip.readFloatGyroY();
//   gz = IMUChip.readFloatGyroZ();
//
//   ax = IMUChip.readFloatAccelX();
//   ay = IMUChip.readFloatAccelY();
//   az = IMUChip.readFloatAccelZ();
// }
//
// void Imu::updateOrientation(){
//   updateRaw();
//   static float deltat = imuFusion.deltatUpdate();
//   imuFusion.MahonyUpdate(gx, gy, gz, ax, ay, az, deltat);
//
//   pitch = imuFusion.getPitch();
//   roll = imuFusion.getRoll();    //you could also use getRollRadians() ecc
//   yaw = imuFusion.getYaw();
// }
//
// float Imu::getTemp(){
//   return IMUChip.readTempC();
// }
//
