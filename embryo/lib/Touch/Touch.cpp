#include <Arduino.h>
#include "Touch.h"




Touch::Touch(){
}

bool Touch::init(bool _debug){
  if (!cap.begin(0x5A)) {
    Serial.println("MPR121 not found, check wiring?");
    return false;
  }
  Serial.println("MPR121 found!");
  debuggingEnabled = _debug;
  return true;
}

void Touch::update(bool _debug){
  currtouched = cap.touched(); //this is mpr121 specific

  for (int i = 0; i < 9; i++) {
    sensor[i].value =  (currtouched & _BV(i) );  //this is mpr121 specific
    if (sensor[i].value <= sensor[i].trigger && sensor[i].touch == false) {
      sensor[i].touch = true;
      sensor[i].touchStart = millis();
      sensor[i].touchEnd = 0;
    } else if (sensor[i].value  >= sensor[i].trigger && sensor[i].touch == true) {
      sensor[i].touch = false;
      sensor[i].touchEnd = millis();
      sensor[i].touchRegistered = false;
    }
    if(_debug || debuggingEnabled){
      Serial.print(String(i) + ":  ");
      Serial.print(sensor[i].value);
      Serial.println("   " + String(sensor[i].touch));
    }

  }
  if(_debug || debuggingEnabled){Serial.println();}
}

bool Touch::touchExclusive(int sensor, int rangeMin, int rangeMax){
  return false;
}

bool Touch::strokeDetect(){
  for (int i = 0; i < numSensors; i++) {
    if (sensor[i].type == "stroke" && sensor[i + 1].type == "stroke") {
      if (!sensor[i].touchRegistered && sensor[i].touchStart < sensor[i + 1].touchStart) {
        duration = (sensor[i + 1].touchStart - sensor[i].touchStart);
        sensor[i].touchRegistered = true;
        if (!stroking) {
          Serial.println("Detected Stroke");
          return true;
        } else {
          Serial.println("Detected Stroke but did nothing");
          return false;
        }
      }
    }
  }
}
