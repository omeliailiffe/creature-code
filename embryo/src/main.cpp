#define DEBUG true

#include <Arduino.h>

#include <Touch.h>
#include <TaskScheduler.h>
#include <WebOTA.h>
#include <Imu.h>
#include "configuration.h"
#include <FastLED.h>


Touch touch;
Imu imu;

const char* ssid = "Its Heckin Windy";
const char* password = "hellotessa";

//Heartbeat stuff
int heartbeatBrightness = 22;
int fadeAmount = 5;
#define HEARTBEATMAX 100
#define HEARTBEATMIN 20
#define BASEBEATINTERVAL 1000
int beatInterval = 1200;

Scheduler schedular;

void tTouchCallback();
void tImuCallback();
void heartbeat();
void heartbeatPulse();
void panic();
void panicDecay();
void LEDs();
Task tTouch(500, TASK_FOREVER, &tTouchCallback);
Task tImu(500, TASK_FOREVER, &tImuCallback);
Task tHeartbeatPulse(BASEBEATINTERVAL, TASK_FOREVER, &heartbeatPulse);
Task tHeartbeat((BASEBEATINTERVAL/2/(HEARTBEATMAX-HEARTBEATMIN)*fadeAmount), TASK_FOREVER, &heartbeat);
Task tPanicDecay(2000, TASK_FOREVER, &panicDecay);
Task tLEDs(100, TASK_FOREVER, &LEDs);

void tTouchCallback(){
  touch.update(true);
}

const int numReadings = 10;

float readings[numReadings];      // the readings from the analog input
int readIndex = 0;              // the index of the current reading
float total = 0;                  // the running total
float average = 0;                // the average

//FASTLED Settings
#define LED_PIN     0
#define NUM_LEDS    12
#define BRIGHTNESS  255
#define LED_TYPE    WS2813
#define COLOR_ORDER GRB

CRGB leds[NUM_LEDS];

void tImuCallback(){
  // imu.dump();
  rawImu temp = imu.getRaw();
  // subtract the last reading:
  total = total - readings[readIndex];
  // read from the sensor:
  readings[readIndex] = abs(temp.gx*temp.gz*temp.gy);
  // add the reading to the total:
  total = total + readings[readIndex];
  // advance to the next position in the array:
  readIndex = readIndex + 1;

  // if we're at the end of the array...
  if (readIndex >= numReadings) {
    // ...wrap around to the beginning:
    readIndex = 0;
  }
  // Serial.println("Average: "+ String(average) +" Threshold: " +String((total / numReadings) - 7));
  if(average < (total / numReadings) - 7){
    Serial.println("Panic: " +String((total / numReadings) - 3-average));
    panic();
    // digitalWrite(32, HIGH);
  } else {
    // digitalWrite(32, LOW);
  }
  // calculate the average:
  average = total / numReadings;
  // send it to the computer as ASCII digits
  // Serial.println(average);
}

int panicLevel = 0;
void panic(){
  panicLevel++;
  if(panicLevel>7){
    panicLevel=7;
  }
  Serial.println("Panic Level: "+ String(panicLevel));
  beatInterval = BASEBEATINTERVAL-((3*panicLevel)*(3*panicLevel));
  tHeartbeat.setInterval((beatInterval/(HEARTBEATMAX-HEARTBEATMIN)*fadeAmount/2));

}
void panicDecay(){
  if(panicLevel>0){panicLevel--;}
  Serial.println("Panic Level: "+ String(panicLevel));
  beatInterval = BASEBEATINTERVAL-((3*panicLevel)*(3*panicLevel));
  tHeartbeat.setInterval((beatInterval/(HEARTBEATMAX-HEARTBEATMIN)*fadeAmount/2));
}

void heartbeat(){
  Serial.println(heartbeatBrightness);

  ledcWrite(0, heartbeatBrightness);
  // ledcWrite(1, 255-heartbeatBrightness);
  // ledcWrite(1, heartbeatBrightness);
  // change the brightness for next time through the loop:
  heartbeatBrightness +=  fadeAmount;

  // reverse the direction of the fading at the ends of the fade:
  if (heartbeatBrightness <= 20 || heartbeatBrightness >= 100) {
    fadeAmount = -fadeAmount;
  }
}

int pulseMode = 0;
void heartbeatPulse(){
  tHeartbeatPulse.setInterval(beatInterval);
  switch (pulseMode) {
    case 0:
      digitalWrite(2, LOW);
      digitalWrite(15, LOW);
      pulseMode++;
      tHeartbeatPulse.setInterval(60);
      break;
    case 1:
      digitalWrite(2, HIGH);
      digitalWrite(15, HIGH);
      pulseMode++;
      tHeartbeatPulse.setInterval(70);
      break;
    case 2:
      digitalWrite(2, LOW);
      digitalWrite(15, LOW);
      pulseMode++;
      tHeartbeatPulse.setInterval(80);
      break;
    case 3:
      digitalWrite(2, HIGH);
      digitalWrite(15, HIGH);
      pulseMode = 0;
      tHeartbeatPulse.setInterval(beatInterval-80-70-60);
      break;
  }
}

extern const TProgmemPalette16 colour_p PROGMEM;
static uint8_t startIndex = 0;

void LEDs(){

  startIndex = startIndex + panicLevel+1; /* motion speed */

  // startIndex = map(panicLevel, 0, MAX_PANIC, 0, 255);
  uint8_t colorIndex = startIndex;
  for( int i = 0; i < NUM_LEDS; i++) {
    leds[i] = ColorFromPalette( colour_p, colorIndex);
    colorIndex += 1+panicLevel;
  }
  FastLED.show();
}

void setup() {

  Serial.begin(115200);
  schedular.init();
  schedular.addTask(tTouch);
  schedular.addTask(tImu);
  schedular.addTask(tHeartbeat);
  schedular.addTask(tHeartbeatPulse);
  schedular.addTask(tPanicDecay);
  schedular.addTask(tLEDs);
  pinMode(2, OUTPUT);
  pinMode(15, OUTPUT);
  pinMode(5, OUTPUT);

  ledcSetup(0, 5000, 12); //heartbeat led
  ledcAttachPin(5, 0);

  // ledcSetup(1, 500, 8); //vibration 1
  // ledcAttachPin(15, 1);
  // ledcWrite(1, 0);

  // ledcSetup(2, 500, 8); //vibration 2
  // ledcAttachPin(2, 2);
  // ledcWrite(2, 0);


  digitalWrite(2, HIGH);
  digitalWrite(15, HIGH);

  FastLED.addLeds<LED_TYPE, LED_PIN, COLOR_ORDER>(leds, NUM_LEDS).setCorrection( TypicalLEDStrip );
  FastLED.setBrightness(  BRIGHTNESS );

  delay(3000);

  if(!touch.init(true)){Serial.println("Touch: Didn't find MPR121");};
  imu.init();

  // tTouch.enable();
  tImu.enable();
  tHeartbeat.enable();
  tHeartbeatPulse.enable();
  tPanicDecay.enable();
  tLEDs.enable();
  // init_wifi(ssid, password, "embryo");
  // webota.init(8080, "/ota");

}

void loop() {
  schedular.execute();
  // webota.handle();
}

const TProgmemPalette16 colour_p PROGMEM =
{

        CRGB::LightGreen,
      CRGB::LightGreen,
      CRGB::SeaGreen,
      CRGB::SeaGreen,

      CRGB::MediumSeaGreen,
      CRGB::MediumSeaGreen,
      CRGB::Teal,
      CRGB::Turquoise,

      CRGB::MediumTurquoise,
      CRGB::LightSkyBlue,
      CRGB::LightSkyBlue,
      CRGB::DeepSkyBlue,

      CRGB::RoyalBlue,
      CRGB::Lavender,
      CRGB::BlueViolet,
      CRGB::DarkMagenta,

};
