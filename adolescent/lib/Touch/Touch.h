#ifndef touch_h
#define touch_h

#include <Arduino.h>
#include "Adafruit_MPR121.h"

#ifndef _BV
#define _BV(bit) (1 << (bit))
#endif

#define SDA 23
#define SCL 19



struct Sensors{
  int pin;
  int trigger;
  int value;
  int touchStart;
  int touchEnd;
  bool touch;
  bool touchRegistered;
  String type;
};

class Touch
{
public:
  Touch();
  bool init(bool _debug=false);
  void update(bool _debug=false);
  bool touchExclusive(int sensor, int rangeMin, int rangeMax);
  bool strokeDetect();
  Sensors sensor[9] =  {
   {1, 1, 0,  0, 0, 0, 0, "stroke"}, //0
   {2, 1, 0, 0, 0, 0, 0, "stroke"}, //1
   {3, 1, 0, 0, 0, 0, 0, "stroke"}, //2
   {4, 1, 0, 0, 0, 0, 0, "stroke"}, //3
   {5, 1, 0, 0, 0, 0, 0, "stroke"}, //4
   {8, 1, 0, 0, 0, 0, 0, "leg"}, //5
   {9, 1, 0, 0, 0, 0, 0, "leg"}, //6
   {10, 1, 0, 0, 0, 0, 0, "leg"}, //7
   {11, 1, 0, 0, 0, 0, 0, "leg"}, //8
  };
  bool stroking;
  int duration;
private:
  Adafruit_MPR121 cap = Adafruit_MPR121();
  uint16_t currtouched;
  int numSensors = 16;
  bool debuggingEnabled = false;
};

#endif
