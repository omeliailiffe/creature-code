#ifndef imu_h
#define imu_h

#include <Arduino.h>
#include <SparkFunLSM6DS3.h>
#include <SensorFusion.h>
#include <Wire.h>

static LSM6DS3 IMUChip(I2C_MODE, 0x6A); //Default constructor is I2C, addr 0x6B
static SF imuFusion;

struct rawImu {
  float gx, gy, gz, ax, ay, az;
};
struct fusionImu {
  float pitch, roll, yaw;
};



class Imu {
public:
  bool init();
  void dump();
  rawImu getRaw();
  fusionImu getOrientation();
  float getTemp();
private:
  float deltat;
};

#endif
