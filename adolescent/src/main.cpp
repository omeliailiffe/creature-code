#define DEBUG true

#include <Arduino.h>

#include <Touch.h>
#include <TaskScheduler.h>
#include <WebOTA.h>
#include <Imu.h>
#include "configuration.h"
#include <ESP32_Servo.h>

#include <FastLED.h>

#define BUILTINLED  5

#if defined ADULT
  #define SSID "Adult"
  #define MDNS "Adult"
  #define PASSWORD "hellotessa"
#elif defined ADOLESCENT
  #define SSID "Adolescent"
  #define MDNS "Adolescent"
  #define PASSWORD "hellotessa"
#endif

Touch touch;
Imu imu;
Servo tentacles;

// const char* ssid = "Its Heckin Windy";
const char* ssid = SSID;
const char* pass = PASSWORD;
const char* mdns = MDNS;

// DEFINE_GRADIENT_PALETTE( ocean_p ) {
// 0,  95,     255,  188,   //Green
// 128, 50,   100,255,  //Blue
// 180, 255, 255, 255,
// 225, 95,   255,188 };
//
// CRGBPalette16 colour_p = ocean_p;
extern const TProgmemPalette16 colour_p PROGMEM;

// //Heartbeat stuff
int heartbeatBrightness = 22;
int fadeAmount = 5;
#define HEARTBEATMAX 100
#define HEARTBEATMIN 20
#define BASEBEATINTERVAL 1000
#define HEARTBEATPIN  17
int beatInterval = 1200;

Scheduler schedular;

void tTouchCallback();
void tImuCallback();
void heartbeat();
void heartbeatPulse();
void panic();
void panicDecay();
void LEDs();
void Servo();

Task tTouch(1000, TASK_FOREVER, &tTouchCallback);
Task tImu(1000, TASK_FOREVER, &tImuCallback);
Task tHeartbeatPulse(BASEBEATINTERVAL, TASK_FOREVER, &heartbeatPulse);
Task tHeartbeat((BASEBEATINTERVAL/2/(HEARTBEATMAX-HEARTBEATMIN)*fadeAmount), TASK_FOREVER, &heartbeat);
Task tPanicDecay(3000, TASK_FOREVER, &panicDecay);
Task tLEDs(100, TASK_FOREVER, &LEDs);
Task tSERVO(300, TASK_FOREVER, &Servo);

void tTouchCallback(){
  touch.update(true);
}

const int numReadings = 10;

float readings[numReadings];      // the readings from the analog input
int readIndex = 0;              // the index of the current reading
float total = 0;                  // the running total
float average = 0;                // the average

void tImuCallback(){
  rawImu temp = imu.getRaw();
  // subtract the last reading:
  total = total - readings[readIndex];
  // read from the sensor:
  readings[readIndex] = abs(temp.gx*temp.gz*temp.gy);
  // add the reading to the total:
  total = total + readings[readIndex];
  // advance to the next position in the array:
  readIndex = readIndex + 1;

  // if we're at the end of the array...
  if (readIndex >= numReadings) {
    // ...wrap around to the beginning:
    readIndex = 0;
  }
  // Serial.println("Average: "+ String(average) +" Threshold: " +String((total / numReadings) - 7));
  if(average < (total / numReadings) - 3){
    Serial.println("Panic: " +String((total / numReadings) - 3-average));
    panic();
  } else {
  }
  average = total / numReadings;
  // Serial.println(average);
}

#define MAX_PANIC 7
int panicLevel = 0;
void panic(){
  panicLevel++;
  if(panicLevel>MAX_PANIC){
    panicLevel=MAX_PANIC;
  }
  Serial.println("Panic Level: "+ String(panicLevel));
  beatInterval = BASEBEATINTERVAL-((3*panicLevel)*(3*panicLevel));
  tHeartbeat.setInterval((beatInterval/(HEARTBEATMAX-HEARTBEATMIN)*fadeAmount/2));

}
void panicDecay(){
  if(panicLevel>0){panicLevel--;}
  Serial.println("Panic Level: "+ String(panicLevel));
  beatInterval = BASEBEATINTERVAL-((3*panicLevel)*(3*panicLevel));
  tHeartbeat.setInterval((beatInterval/(HEARTBEATMAX-HEARTBEATMIN)*fadeAmount/2));
}

void heartbeat(){
  ledcWrite(0, heartbeatBrightness);
  // change the brightness for next time through the loop:
  heartbeatBrightness +=  fadeAmount;
  // reverse the direction of the fading at the ends of the fade:
  if (heartbeatBrightness <= 20 || heartbeatBrightness >= 100) {
    fadeAmount = -fadeAmount;
  }
}

int pulseMode = 0;
void heartbeatPulse(){
  switch (pulseMode) {
    case 0:
      Serial.println("Pulsing");
      digitalWrite(HEARTBEATPIN, LOW);
      pulseMode++;
      tHeartbeatPulse.setInterval(60);
      break;
    case 1:
      digitalWrite(HEARTBEATPIN, HIGH);
      pulseMode++;
      tHeartbeatPulse.setInterval(70);
      break;
    case 2:
      digitalWrite(HEARTBEATPIN, LOW);
      pulseMode++;
      tHeartbeatPulse.setInterval(80);
      break;
    case 3:
      digitalWrite(HEARTBEATPIN, HIGH);
      pulseMode = 0;
      tHeartbeatPulse.setInterval(beatInterval-80-70-60);
      break;
  }
  // digitalWrite(HEARTBEATPIN, LOW);
  // delay(60);
  // digitalWrite(HEARTBEATPIN, HIGH);
  // delay(50);
  // digitalWrite(HEARTBEATPIN, LOW);
  // delay(80);
  // digitalWrite(HEARTBEATPIN, HIGH);
}

static uint8_t startIndex = 0;

//FASTLED Settings
#define LED_PIN1  15
#define LED_PIN2  2
#define NUM_LEDS    20
#define BRIGHTNESS  155
#define LED_TYPE    WS2812B
#define COLOR_ORDER  GRB

CRGB leds[NUM_LEDS];

CRGBPalette16 currentPalette;

void LEDs(){

  startIndex = startIndex + panicLevel+1; /* motion speed */

  uint8_t colorIndex = startIndex;
  for( int i = 0; i < NUM_LEDS; i++) {
    leds[i] = ColorFromPalette( colour_p, colorIndex);
    colorIndex += 1+panicLevel;
  }
  FastLED.show();
}


int servoPostion = 1;
int servoDelta = 1;
#define SERVO_PIN 4
#define SERVO_MAX 80
#define SERVO_MIN 30

void Servo(){
  Serial.println(servoPostion);
  servoPostion += random(SERVO_MAX/(10-panicLevel))*servoDelta;
  if(servoPostion > SERVO_MAX){
    servoDelta = -1;
    servoPostion = SERVO_MAX;
  } else if (servoPostion < SERVO_MIN){
    servoDelta = 1;
    servoPostion = SERVO_MIN;
  }
  tentacles.write(servoPostion);
}

void setup() {

  Serial.begin(115200);

  // init_wifi(ssid, pass, mdns, true);
  // webota.init(8080, "/ota");
  // delay(7000);

  schedular.init();
  schedular.addTask(tTouch);
  schedular.addTask(tImu);
  schedular.addTask(tHeartbeat);
  schedular.addTask(tHeartbeatPulse);
  schedular.addTask(tPanicDecay);
  schedular.addTask(tLEDs);
  schedular.addTask(tSERVO);

  pinMode(HEARTBEATPIN, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(SERVO_PIN, OUTPUT);
  //
  ledcSetup(0, 5000, 12); //heartbeat led
  ledcAttachPin(BUILTINLED, 0);

  FastLED.addLeds<LED_TYPE, LED_PIN1, COLOR_ORDER>(leds, NUM_LEDS).setCorrection( TypicalLEDStrip );
  FastLED.addLeds<LED_TYPE, LED_PIN2, COLOR_ORDER>(leds, NUM_LEDS).setCorrection( TypicalLEDStrip );
  FastLED.setBrightness(  BRIGHTNESS );
  fill_solid(leds, NUM_LEDS, CRGB::Black);

  tentacles.attach(SERVO_PIN);

  delay(100);

  if(!touch.init(true)){Serial.println("Touch: Didn't find MPR121");};
  imu.init();

  // tTouch.enable();
  tImu.enable();
  tHeartbeat.enable();
  tHeartbeatPulse.enable();
  tPanicDecay.enable();
  tLEDs.enable();
  // tSERVO.enable();



}

void loop() {
  schedular.execute();
  // webota.handle();
}

const TProgmemPalette16 colour_p PROGMEM =
{

      CRGB::LightGreen,
    CRGB::LightGreen,
    CRGB::SeaGreen,
    CRGB::SeaGreen,

    CRGB::MediumSeaGreen,
    CRGB::MediumSeaGreen,
    CRGB::Teal,
    CRGB::Turquoise,

    CRGB::MediumTurquoise,
    CRGB::LightSkyBlue,
    CRGB::LightSkyBlue,
    CRGB::DeepSkyBlue,

    CRGB::RoyalBlue,
    CRGB::Lavender,
    CRGB::BlueViolet,
    CRGB::DarkMagenta,


};
